﻿using System.CommandLine;
using Service.Countdown;

var rootCommand = new RootCommand("Command line timer");
rootCommand.AddCommand(
    new CountdownCommand(
        new Command[]{
            new CreateCommand()
        }
    )
);

await rootCommand.InvokeAsync(args);