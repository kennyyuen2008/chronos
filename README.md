# Chronos

Chronos is a CLI tool for countdown and alarm.

## Contributing

We would love community contributions here.

This project has adopted the code of conduct defined by the [Contributor Covenant](http://contributor-covenant.org/) 
to clarify expected behavior in our community. For more information, see the [.NET Foundation Code of Conduct](http://www.dotnetfoundation.org/code-of-conduct).

## License

This project is licensed with the [MIT license](LICENSE).

## Related Projects

You should take a look at these related projects:

- [.NET Core](https://github.com/dotnet/core)
