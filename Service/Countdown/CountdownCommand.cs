namespace Service.Countdown;

using System.CommandLine;

public class CountdownCommand : Command, IServiceCommand
{
    private readonly Command[]? _commands;

    public CountdownCommand(Command[] commands) : base("countdown", "Countdown mode.")
    {
        _commands = commands;

        if (_commands != null)
        {
            foreach (var command in _commands)
            {
                AddCommand(command);
            }
        }
    }
}