namespace Service.Countdown;
using System.CommandLine;

public class CreateCommand : Command, ICountdownCommand
{
    private readonly Action<int, int>? _handler;

    public CreateCommand(Action<int, int>? handler = null) : base("create", "Create a countdown timer.")
    {
        _handler = handler;

        var minuteOption = new Option<int>(
                    name: "--minute",
                    description: "Minute for the timer.");
        minuteOption.AddAlias("-m");

        var secondOption = new Option<int>(
                    name: "--second",
                    description: "Second for the timer.");
        secondOption.AddAlias("-s");

        Add(minuteOption);
        Add(secondOption);

        this.SetHandler(_handler != null ? _handler :
                    new Action<int, int>(
                        (int minute, int second) =>
                        {
                            Console.WriteLine($"Minute: {minute}, Second: {second}");
                        }
                    ),
                    minuteOption, secondOption);
    }
}